mainApp.factory('sharingService', [function(){
    var dataobject = {};
    var taskData = {};
    var showDiv = false;
    var showdatadiv = false;
    return{
        "getData":function(){
            return dataobject;
        },
        "setData":function(arg){
            dataobject = arg;
        },
        
        "getTaskData":function(){
            return taskData;
        },
        "setTaskData":function(arg){
            taskData = arg;
        },
        "setShowDiv":function(arg){
            showdatadiv = arg;
        },
        "getShowDiv":function(){
            return showdatadiv;
        }
        
    }
    
}]);