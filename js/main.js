var mainApp = angular.module("mainApp", ['ngRoute',]);

mainApp.config(function($routeProvider) {
	
    $routeProvider
		.when('/itemsList', {
			templateUrl: 'view/itemsList.html',
			controller: 'itemsListController'
		})
		.when('/additem', {
			templateUrl: 'view/additem.html',
			controller: 'additemController'
		})
		.otherwise({
			templateUrl: 'view/itemsList.html',
			controller: 'itemsListController'
        });
});

mainApp.controller("showDiv",["$scope","sharingService",function($scope,sharingService){
    

    
}]);
