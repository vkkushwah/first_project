mainApp.controller('additemController', function($http,$scope,sharingService,$location) {
	
	$scope.saveItem=function(item){
        
        var date = new Date(item.expectedSellingDate);
        item.expectedSellingDate=""+(date.getMonth() + 1)+"/"+date.getDate+"/"+date.getFullYear();
        
       
        var fd = new FormData();
        fd.append('file', $scope.myFile);
        fd.append('item',angular.toJson(item,true));
        $http.post("http://localhost:8080/Product_Management_System/additem/", fd, {
            transformRequest : angular.identity,
            headers : {
                'Content-Type' : undefined
            }
        }).success(function() {
            if(status==201){
				$scope.itemMessage="Item Has Been Added Successfully !!";
			}else {
				$scope.itemMessage="This Item Is Already Exist !!";
			}
        }).error(function() {
            $scope.itemMessage=" Something Wrong !!"
        });
    };
});

mainApp.directive('fileModel', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function(scope, element, attrs) {
	            var model = $parse(attrs.fileModel);
	            var modelSetter = model.assign;

	            element.bind('change', function(){
	                scope.$apply(function(){
	                    modelSetter(scope, element[0].files[0]);
	                });
	            });
	        }
	    };
}]);